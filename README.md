# Riverty Magento Extension #
## Version: 3.3 ##

### Information for installing or support can be found at https://developer.riverty.com/, or at plugins@arvato.com ###

** Release notes, version 3.3: **

* Customers in the Netherlands can now enjoy our latest payment option, 'Pay in 3'.
* We're constantly striving to enhance the checkout process, and as such, we've made it even smoother by removing checkboxes for the Terms and Conditions and eliminating the Gender form field.
* The Riverty PHP Library has been upgraded to version 3.8.0, providing improved performance and stability.
* To ensure we always have the latest legal information, we now source it directly from the available payment methods API call.
* We've resolved a translation issue in the frontend, ensuring a seamless and accurate user experience.
* In the backend configuration, we've refined the naming of payment methods to make them easier to identify and manage.

** Release notes, version 3.2: **

* DP-1129 - Implemented available payment methods in the checkout for REST payment methods.
* DP-1111 - Added plugin provider data in API requests.
* DP-1144 - Update the AfterPay PHP Library to version 3.6.0.
* DP-1193 - Checked and changed titles and taglines for SOAP payment methods.

** Release notes, version 3.1: **

* DP-950 - Updated the AfterPay PHP Library to version 3.5.1
* DP-976 - Added the possibility for merchant specific terms and conditions
* DP-1010 - Updated the logo.

** Release notes, version 3.0: **

* DP-515 - Update the AfterPay PHP Library to version 2.2
* DP-637 - Update the AfterPay PHP Library to version 2.4
* DP-609 - Sent in full first name instead of initials for NL and BE requests
* DP-614 - Remove pending/push functionality for Belgium
* DP-222 - Add SVG logo from CDN
* DP-107 - Add Call to action to payment methods configuration
* DP-519 - Birthday field is not activated by default for Belgium Digital Invoice
* DP-202 - Terms and conditions should always be visible and not configurable anymore.
* DP-394 - Removed advisoryprocess from the payment methods.
* DP-636 - Fixed issue with refunding fixed price bundles.
* DP-489 - Added French translations of frontend fields, rejections and validations for French speaking customers in Belgium
* DP-622 - Added groupId element for REST requests.
* DP-572 - Added profile tracking to german payment methods.
* DP-494 - Added sandbox connection for REST payment methods.
* DP-315 - Added product images and urls to authorization request.
* DP-571 - Checked compatibility with shipment provider SendCloud, MyParcel and PostNL
* DP-298 - Fixed problem with submitting multiple bundled products.
* DP-615 - Added NL and BE payment methods through REST.

** Release notes, version 2.9: **

* DP-489 - Added French translations of frontend fields, rejections and validations for French speaking customers in Belgium
* DP-509 / DP-572 - Implemented profile tracking for DACH payment methods
* DP-531 - Compatibility with customer groups assigned by VAT number validation.

** Release notes, version 2.8: **

* DP-456 - Add new urls for terms and conditions and privacy statement
* DP-327 - Use new terms and conditions and privacy statement from CDN
* DP-498 - Fixed issue with partial refunds in Germany
* DP-471 - Check auto invoice and capturing default settings. Seems that one of the latest updates affects the state of orders
* DP-407 - By default show terms and conditions checkbox

** Release notes, version 2.7: **

* DP-463 - Translate message when wrong Chamber of Commerce number is used
* DP-461 - Address Correction - Notice for corrected Delivery Address is wrong

** Release notes, version 2.6.1: **

* DP-459 - Make use of functionality of negative adjustments in refund
* DP-458 - Minimum amount of showing payment method only works when maximum amount is filled in
* If vat amount is not set, set a default of 0 to avoid empty value notices
* Correct default value for automatic invoicing

** Release notes, version 2.6: **

* DP-138 - Add installments for Germany and Austria
* DP-188 - Add Direct Debit for Germany
* DP-198 - Add Direct Debit for Austria
* DP-143 - Prepare installments for Sweden
* DP-185 - Prepare installments for Finland
* DP-187 - Prepare installments for Norway

** Release notes, version 2.5: **

* DP-40  - Compatibility check latest version IWD Checkout Module
* DP-309 - Change Payment method description to display guidelines
* DP-317 - Bug, Austria is now the default country for Dutch payment methods on a clean install

** Release notes, version 2.4.1: **

* Updated AfterPay PHP Library to version 1.7
* DP-281 - Bugfix for compatibility with One Step Checkout and Germany

** Release notes, version 2.4: **

* DP-261 - Fixed problem with showing configuration fields (PHP warning was showing instead)
* DP-260 - Fixed problem with refunding in Germany resuling in memory and timeout error.
* DP-275 - Fixed bug showing notice: Undefined variable: country
* Added 2018 to copyright notice
* DP-197 - Added new ways to get the correct IP address and added support for Cloudflare IP addresses.
* DP-258 - Implemented new translations for Denmark

** Release notes, version 2.3: **

* Updated to AfterPay Library 1.6.0
* Bugfix on typo with paymentmethodcode
* DP-26 - Add AfterPay Austria (open invoice) as payment method
* DP-27 - Add AfterPay Switzerland (open invoice) as payment method
* Added new field for social security number, needed for the following additions:
* DP-142 - Add AfterPay Sweden (open invoice) as payment method
* DP-180 - Add AfterPay Finland (open invoice) as payment method
* DP-181 - Add AfterPay Denmark (open invoice) as payment method
* DP-181 - Add AfterPay Norway (open invoice) as payment method
* DP-117 - Updated sort order options
* DP-207 - Fix to send the correct city of the 'pakje_gemak' pickup point
* DP-213 - Make address correction also respond on return code 200.101

** Release notes, version 2.2.1: **

* DP-79 - Replaces + sign in phone number for 00
* DP-198 - Correct German translation for validation message on phone number

** Release notes, version 2.2: **

* DP-123 - Translation update
* DP-121 - Added (sub)merchant id to German portfolio to improve support for multishops
* DP-131 - Tested compatibility with Magento 1.9.3.7

** Release notes, version 2.1: **

* DP-108 - Updated to AfterPay Library 1.4.0
* DP-17 - Tested compatibility with Patch SUPEE-10266 (Magento 1.9.3.6)
* DP-21 - DE: Sending VAT amounts in the authorisation and refund request
* DP-116 - BE: Solved bug which caused the inability to capture Belgian orders
* DP-120 - Solved bug which caused problems with sending additional information

** Release notes, version 2.0: **

* Updated to AfterPay Library 1.2.9
* Added new dedicated payment methods for all Dutch, Belgian and German payment methods
* Added Dutch and German translations
* Added German address feedback
* Removed pregmatch from description because of many special characters in Germany
* Improved way to sent mails based on Magento standards (ticket #244)
* Show footnote on German payment method
* Add setRest method to enable REST communication
* Make terms and conditions configurable for Germany
* Updated refund method, removed partial dependency method because all refunds are partial refunds
* Set default gender in Netherlands to Woman
* If gender is set in default Magento, use that, else use from AfterPay form
* Germany needs no date of birth
* If discount is calculated in Magento, use seperate rule with explanation only in Netherlands
* Set margin of check on full invoices to 5 cents
* Get prefix for compatiblity with Paazl
* Cleaned up code to match coding standards
* Use new debug functionality
* Changed address line in German address feedback
* Checked compatibility with latest FireCheckout Module 3.11.2 (#243)
* Bugfixed observer, problem with not watching right payment methods

** Release notes, version 1.7.0: **

* Updated to AfterPay Library 1.2.2
* Replaced Soap Model with API model for future REST operations
* Added AfterPay Library from composer in the /lib folder
* No birthday needed for DE
* Make gender optional for DE
* Add new text for german address correction
* Cleaned up Dutch translations
* Added German translations

** Release notes, version 1.6.8: **

* Show portfolio based on ip(s): now part of portfolio settings
* New: hide portfolio based on ip(s). Useful when using AfterPay in-store (you need a separate portfolio)
* Removed: ip restriction in general settings
* Send pick-up address as delivery address, when using POSTNL Pakjegemak pick-up option. This work with both the POSTNL and MyParcel extension
* Bugfix: Fixed typo for rejection messages with Belgian phonenumber
* Bugfix: Items of bundled products with options get correct vat data

** Release notes, version 1.6.7: **

* Fixes: Memory problem in order proces
* Fixes: Copyright to 2017
* Fixes: Typo in error for capturing
* Changes: Removed reward points from refund

**Release notes, version 1.6.6:**

* Features: Compatible with Magento CE 1.9.3.1
* Fixes: Payment method description now visible on invoice PDF
* Fixes: More improvements for sending the correct IP when using (caching) proxy’s
* Fixes: Layout fixes when using FireCheckout
* Fixes: PHP Notice error in combination with bankaccountnumber solved

**Release notes, version 1.6.4:**

* Fixes: fixed way of getting the service fee amount before sending to AfterPay

**Release notes, version 1.6.3:**

* Fixes: Added improvement for validation errors in Belgium
* Fixes: Added improvement for bundled products
* Additions: Added new way to strip special characters from soap request

**Release notes, version 1.6.2:**

* Fixes: Fee Amount and description in PDF now correct
* Fixes: Compatibility fix for Idev_OneStepCheckout, Fee amount not doubled
* Changes: Title of payment fee is now 'Afterpay Servicekosten' instead of 'Afterpay Fee'
* Fixes: Fixed redirect issue caused by empty array and causing less showing of error messsages
* Additions: Added posibility to enable sending of invoices
* Fixes: Replaced some empty() functions because of backwards compatibility
* Changes: Added Fee amount and description to transactional emails
* Fixes: Problem with TIG PostNL - check if config_state is available before if statement
* Fixes: Fixed problem with refunding Bundled product with fixed price
* Fixes: Added check to generation of totals if there are shipping totals (in case of virtual products)
* Fixes: Fixed problem with products containing parents (bundles and configurables) which have different vat calculations
* Fixes: Problem fixed on refunds for orders with discounts
* Compatibility check: IWD One Page Checkout (IWD_Opc) - 4.3.0
* Compatibility check: Idev One Step Checkout (Idev_OneStepCheckout) - 4.5.5
* Compatibility check: PostNL (TIG_PostNL) - 1.7.2
* Compatibility check: Buckaroo (TIG_Buckaroo3Extended) - 4.15.2
* Compatibility check: MultiSafePay (MultiSafepay_Msp) - 2.2.7
* Compatibility check: Fooman Surcharge (Fooman_Surcharge) - 3.1.12
* Compatibility check: Fooman PDF (Fooman_PdfCustomiser) - 2.12.1

**Release notes, version 1.6.1:**

* Removed unneeded B2B fields
* Compatibility test and layout fix with IWD Opc (IWD One Page Checkout), version 4.3.0
* Compatibility test and layout fix with Idev OneStepCheckout, version 4.5.5
* Option to hide payment method based on ip (reversed ip restriction)
* Fix for unneeded orderlines when service fee is not used
* Improved IP filter (now makes use of REMOTE_ADDR, HTTP_X_FORWARDED_FOR and HTTP_X_REAL_IP)

**Release notes, version 1.6.0:**

* Removed general payment fee from main module
* Create new basic AfterpayFee Submodule

**Release notes, version 1.5.8:**

* Added compatibility with Fooman Surcharge 3.1.12

**Release notes, version 1.5.7:**

* Renamed CaptureStarted to AfterpayCaptureStarted to prevent naming issues
* Removed Capture Delay Days
* Added new functionality for capturing to create the invoice but don´t capture it immeadiatly

**Release notes, version 1.5.6:**

* Possibility to redirect to different page after rejection or validation error
* Possibility to enable or disable AfterPay Checkout Fields
* Fix for using a comma value in service fee
* Fix for using percentage in service fee
* Added compatibility fix for 1.6 (not having getProduct on request)
* Added new logo
* Tested compatibility with Magento 1.9.2.3

**Release notes, version 1.5.5:**

* Major update on capture triggers, now you can trigger the capture on shipping and on magento status
* Fixed problem with using AfterPay for backend orders
* Fixed problem sending order mails in Magento EE (because of version numbering check)
* Hotfix for rounding errors with service fee in Magento EE
* Fixed problem which bundled product errors

**Release notes, version 1.5.4:**

* Fixed problem with not supported array_search php function
* Fixed enterprise logic which causes double service fee in Enterprise environments
* Fixed enterprise check which causes problems with sending order mails

**Release notes, version 1.5.3:**

* Fixes: Fixed problem with template file showing portfolio information
* Fixes: Fixed problem showing the correct vat amount on the service fee in the backend on orders, invoices, credit notes and pdf
* Fixes: Fixed posibility for using the adjustment fees when refunding in Magento
* Fixes: Fee not showing in One Step Checkout, when AfterPay is selected, but shipping is not.
* Removals: Removed AfterPay logo, supporttab and coloring information in adminhtml
* Additions: Created extensive support for bundled products with flexible pricing. Send in detail with the correct vat category

**Release notes, version 1.5.2:**

* Changes: Compatible with Magento 1.9.2.2
* Changes: Compatible with IWD Onestepcheckout 4.08
* Fixes: Fixed vat category on refund with discount 
* Fixes: Fixed view in IDEV Onestepcheckout (including vat and after shipping)
* Additions: The following order line is only showed when the order contains discount: ‘De stuksprijs is incl. eventuele korting’
* Fixes: Fixed double order confirmation mails
* Fixes: Fixed calculation on service fee when percentage is used
* Additions: Possibility to add status to failed captures
* Fixes: support for extra refund fields (refund amount and refund fee)
* Fixes: Compatibility increase with the PostNL and Buckaroo Module
* Fixes: several small bugfixes and language improvements

**Release notes, version 1.4.0:**

* Changes: Compatible with Magento 1.9.2.0
* Changes: Compatible with Magento Security Update (PATCH_SUPEE-6285_CE_1.9.1.1_v1 en PATCH_SUPEE-6285_CE_1.9.1.1_v2)
* Changes: Compatible with IWD Onestepcheckout 4.08
* Changes: Compatible with TIG_PostNL Module (probleem with service fee)
* Changes: Structural naming change (TIG_Afterpay now Afterpay_Afterpay).
* Changes: Removed non-risk posibility.
* Fixes: Improvement in testing with IP restriction
* Changes: SOAP endpoint changed from api.afterpay.nl to mijn.afterpay.nl
* Addition: New trademarks added to back and frontend
* Fixes: Fix for order confirmation mail from version 1.9.1.0.
* Changes: Merchant ID on portefolio setting instead of general settings
* Changes: Modus (test/live) only on portfolio setting
* Fixes: Refund on alternative merchant ID
* Changes: Improvement in sending IP adresses for loadbalancers and Cloudflare environments
* Fixes: Servicekosten view improvement 
* Fixes: several small bugfixes and language improvements